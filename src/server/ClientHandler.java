package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;

import client.ClientResponseManager;
import storage.StorageAdmin;

public class ClientHandler implements Runnable {
	Socket clientSocket;
	String ip;
	boolean isRunning = true;
	
	BufferedWriter writer = null;
	BufferedReader reader = null;
	
	void close() {
		try {
			clientSocket.close();
		} catch (Exception e) {}
	}

	public ClientHandler(Socket socket) {
		clientSocket = socket;
		ip = clientSocket.getRemoteSocketAddress().toString();
	}
	
	public String getClientIP() {
		return ip.substring(1);
	}

	@Override
	public void run() {
		try {
			writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8));
			reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		} catch (IOException e) {
			System.out.println("Blad tworzenia strumieni");
			return;
		}
		
		String line = null;
		System.out.println("ClientHandler");
		while(isRunning) {
			try {
				line = reader.readLine();
				System.out.println("server read line: " + line);
				
				if (line == null) {
					System.out.println("client disconnect");
					break;
				}
				processInput(line);

			} catch (Exception e) {}
		}
		try {			
			clientSocket.close();
		} catch (IOException e) {
			clientSocket = null;
		}
		isRunning = false;
		System.out.println("client done");
	}
	
	void processInput(String line) {
		System.out.println("PROCESS INPUT");
		Gson gson = new Gson();
		ClientResponseManager p = gson.fromJson(line, ClientResponseManager.class);
		StorageAdmin.serverSaveResponse(p);
		System.out.println("Odebrano odpowiedz");
	}
	public void send(String line) {
	    try {
	      writer.write(line);
	      writer.newLine();
	      writer.flush();
	      System.out.println("Serwer wysłano: " + line);
	    }
	    catch(IOException e) {}
	  }
}