package server;

import javax.swing.JFrame;	
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.Answer;
import data.Poll;
import data.Question;
import data.Question.QuestionType;
import storage.StorageAdmin;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

// Okno tworzenia/modyfikacji ankiety
@SuppressWarnings("serial")
public class AddOrModifyPollFrame extends JFrame {

	private JPanel contentPane;
	private JTextField txtQuestion; // Tresc pytania
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtAnswer; // Tresc odpowiedzi
	private JTextField txtPollName; // Nazwa ankiety
	JList<String> lstQuestions; // Lista odpowiedzi
	JList<String> lstAnswers; // Lista pytan
	DefaultListModel<String> listModelQ, listModelA;
	Poll p; // Klasa ankiety
	private JRadioButton rdbtnSingle;
	private JRadioButton rdbtnMulti;
	
	void initPoll() {
		if (p == null) {
			p = new Poll();
		}
	}
	
	// Wyczysc okno
	void clearForm() {
		listModelA.clear();
		listModelQ.clear();
		resetAnswerInput();
		resetQuestionInput();
		resetPollName();
		p = null;
		initPoll();
	}
	
	void resetPollName() { setPollName(""); }
	String getPollNameFromForm() { return txtPollName.getText(); }
	void setPollName (String s) { txtPollName.setText(s); }

	String getQuestionInput() { return txtQuestion.getText(); }
	void   setQuestionInput(String s) { txtQuestion.setText(s); }

	void resetQuestionInput() { 
		setQuestionInput(""); 
		resetQuestionType();
	}
	
	void selectSingleQuestionType() {
		buttonGroup.setSelected(rdbtnSingle.getModel(), true);
	}
	
	void selectMultiQuestionType() {
		buttonGroup.setSelected(rdbtnMulti.getModel(), true);
	}
	
	void resetQuestionType() { buttonGroup.clearSelection(); }
	
	QuestionType getCheckedQuestionType() {
		if (rdbtnMulti.isSelected()) { 
			return QuestionType.MULTIPLE; }
		else if (rdbtnSingle.isSelected()) {
			return QuestionType.SINGLE; }
		else return QuestionType.UNKNOWN;
	}

	// Indeks zaznaczonego pytania
	// Brak, zwraca -1
	int getSelectedQuestionIndex() {
		int index = lstQuestions.getSelectedIndex();
		if (index == -1) return -1;
		return index;
	}

	// Zmien typ pytania
	void changeQuestionType(QuestionType type) {
		Question q = getSelectedQuestion();
		if (q == null) return;
		
		q.setType(type);
	}

	// Obecne pytanie
	Question getSelectedQuestion() {
		int index = getSelectedQuestionIndex();
		if (index == -1) return null;
		return p.getQuestionAtIndex(index);
	}
	
	void addQuestionToPoll() {
		if (getQuestionInput() == null) return; // Brak tekstu
		if (getCheckedQuestionType() == QuestionType.UNKNOWN) return; // Brak typu
		if (getSelectedQuestion() != null) return; // Brak pytania

		Question q = new Question();
		q.setContent(getQuestionInput());
		q.setType(getCheckedQuestionType());
		
		p.addQuestion(q);
		listModelQ.addElement(q.getContent());
		
		resetQuestionInput();
		resetQuestionType();
	}
	
	void removeQuestion() {
		int index = getSelectedQuestionIndex();
		if (index == -1) return;
		
		p.removeQuestion(index);
		listModelQ.remove(index);
	}

	// Pokaz informacje o pytaniu
	void setQuestionInfo() {
		Question q = getSelectedQuestion(); // Obecnie pytanie
		if (q == null) {
			resetQuestionInput();
			refreshAnswers();
			return;
		}
		
		txtQuestion.setText(q.getContent());

		resetQuestionType();
		switch(q.getType()) {
		case SINGLE: selectSingleQuestionType(); break;
		case MULTIPLE: selectMultiQuestionType(); break;
		default:
			break;
		}
		refreshAnswers();
	}
	
	// Zaaktualizuj tresc pytania
	// 1. W samym pytaniu
	// 2. W liscie pytan
	void changeQuestionContent() {
		Question q = getSelectedQuestion();
		if (null == q) return;
		
		q.setContent(getQuestionInput());
		int index = getSelectedQuestionIndex();
		listModelQ.set(index, q.getContent());
	}

	void refreshQuestions() {
		listModelQ.clear();
		for (Question q : p.getQuestions()) {
			listModelQ.addElement(q.getContent());
		}
	}
	/*
	 *  Odpowiedzi
	 */
	
	void refreshAnswers() {
		listModelA.clear();
		for (Answer a : getSelectedQuestion().getAnswers()) {
			listModelA.addElement(a.getContent());
		}
	}

	int getSelectedAnswerIndex() {
		int index = lstAnswers.getSelectedIndex();
		if (index == -1) return -1;
		return index;
	}
	
	Answer getSelectedAnswer() {
		if (getSelectedQuestion() == null) return null;
		int answerIndex = getSelectedAnswerIndex();
		if (answerIndex == -1) return null;
		
		return getSelectedQuestion().getAnswers().get(answerIndex);
	}
	void addAnswerToPoll() {
		if (getAnswerInput() == null) return; // Brak tekstu
		
		int questionIndex = getSelectedQuestionIndex();
		if (questionIndex == -1) return; // Brak zaznaczonego pytania
		
		Answer a = new Answer();
		a.setContent(getAnswerInput());
		
		p.getQuestionAtIndex(questionIndex).getAnswers().add(a);
		listModelA.addElement(a.getContent());
		
		resetAnswerInput();
	}
	void removeAnswer() {
		int question = getSelectedQuestionIndex();
		if (question == -1) return;
		int index = getSelectedAnswerIndex();
		if (index == -1) return;
		
		p.getQuestionAtIndex(question).getAnswers().remove(index);
		listModelA.remove(index);
		
		resetAnswerInput();
	}
	void changeAnswerContent() {
		Question q = getSelectedQuestion();
		if (null == q) return;
		int answer = getSelectedAnswerIndex();
		if (answer == -1) return;
		
		q.getAnswers().get(answer).setContent(getAnswerInput());
		
		listModelA.set(answer, q.getAnswers().get(answer).getContent());
	}

	void setAnswerInfo() {
		Answer a = getSelectedAnswer();
		if (a == null) return;
		
		txtAnswer.setText(a.getContent());	
	}

	String getAnswerInput() { return txtAnswer.getText(); } 
	void setAnswerInput(String s) { txtAnswer.setText(s); }
	void resetAnswerInput() { setAnswerInput("");}

	void savePoll() {
		// TODO WALIDACJA?
		p.setTitle(getPollNameFromForm());
		p.recalcIDs();
		StorageAdmin.savePoll(p);
		clearForm();
	}

	/**
	 * Create the frame.
	 */
	public AddOrModifyPollFrame() {	
		initPoll();
		setAlwaysOnTop(true);
		setTitle("Generator ankiet");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 652, 478);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 80, 238, 260);
		contentPane.add(scrollPane);
		
		listModelQ = new DefaultListModel<String>();
		lstQuestions = new JList<String>(listModelQ);
		lstQuestions.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() >= 2) {
					lstQuestions.clearSelection();
				}
			}
		});
		lstQuestions.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				lstQuestions.clearSelection();
			}
		});
		lstQuestions.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				boolean adjust = arg0.getValueIsAdjusting();
				if (!adjust && lstQuestions.getSelectedIndex() >= 0) {
					setQuestionInfo();
				}
			}
		});
		scrollPane.setViewportView(lstQuestions);
		lstQuestions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JButton btnAddQuestion = new JButton("Dodaj");
		btnAddQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addQuestionToPoll();
			}
		});
		btnAddQuestion.setBounds(10, 350, 98, 23);
		contentPane.add(btnAddQuestion);
		
		JButton btnDeleteQuestion = new JButton("Usuń");
		btnDeleteQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeQuestion();
			}
		});
		btnDeleteQuestion.setBounds(118, 350, 89, 23);
		contentPane.add(btnDeleteQuestion);
		
		txtQuestion = new JTextField();
		txtQuestion.setBounds(10, 384, 238, 20);
		contentPane.add(txtQuestion);
		txtQuestion.setColumns(10);
		txtQuestion.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				changeQuestionContent();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				changeQuestionContent();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				changeQuestionContent();
			}
		});
		
		JLabel lblNewLabel = new JLabel("Typ pytania:");
		lblNewLabel.setBounds(258, 54, 79, 23);
		contentPane.add(lblNewLabel);
		
		rdbtnSingle = new JRadioButton("Pojedyncze");
		rdbtnSingle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeQuestionType(QuestionType.SINGLE);
			}
		});
		buttonGroup.add(rdbtnSingle);
		rdbtnSingle.setBounds(343, 54, 109, 23);
		contentPane.add(rdbtnSingle);
		
		rdbtnMulti = new JRadioButton("Wielokrotne");
		rdbtnMulti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeQuestionType(QuestionType.MULTIPLE);
			}
		});
		buttonGroup.add(rdbtnMulti);
		rdbtnMulti.setBounds(343, 78, 109, 23);
		contentPane.add(rdbtnMulti);
		
		JLabel lblListaPyta = new JLabel("Lista pytań:");
		lblListaPyta.setBounds(20, 54, 68, 21);
		contentPane.add(lblListaPyta);
		
		JLabel lblOdpowiedzi = new JLabel("Odpowiedzi:");
		lblOdpowiedzi.setBounds(258, 143, 72, 23);
		contentPane.add(lblOdpowiedzi);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setBounds(309, 177, 317, 164);
		contentPane.add(scrollPane_1);
		
		listModelA = new DefaultListModel<String>();
		lstAnswers = new JList<String>(listModelA);
		lstAnswers.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getClickCount() >= 2) {
					lstQuestions.clearSelection();
				}
			}
		});
		lstAnswers.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				lstAnswers.clearSelection();
			}
		});
		lstAnswers.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				boolean adjust = arg0.getValueIsAdjusting();
				if (!adjust && lstAnswers.getSelectedIndex() >= 0) {
					setAnswerInfo();
				}
			}
		});
		scrollPane_1.setViewportView(lstAnswers);
		lstAnswers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		txtAnswer = new JTextField();
		txtAnswer.setBounds(309, 384, 317, 20);
		contentPane.add(txtAnswer);
		txtAnswer.setColumns(10);
		txtAnswer.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				changeAnswerContent();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				changeAnswerContent();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				changeAnswerContent();
			}
		});
		
		JButton btnAddAnswer = new JButton("Dodaj");
		btnAddAnswer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addAnswerToPoll();
			}
		});
		btnAddAnswer.setBounds(309, 350, 89, 23);
		contentPane.add(btnAddAnswer);
		
		JButton btnDeleteAnswer = new JButton("Usuń");
		btnDeleteAnswer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAnswer();
			}
		});
		btnDeleteAnswer.setBounds(408, 350, 89, 23);
		contentPane.add(btnDeleteAnswer);
		
		txtPollName = new JTextField();
		txtPollName.setBounds(10, 11, 450, 36);
		contentPane.add(txtPollName);
		txtPollName.setColumns(10);
		
		JButton btnSavePoll = new JButton("Zapisz ankiete");
		btnSavePoll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				savePoll();
			}
		});
		btnSavePoll.setBounds(458, 78, 165, 88);
		contentPane.add(btnSavePoll);
	}

	public AddOrModifyPollFrame(String name) {
		this();
		p = StorageAdmin.getServerPoll(name);
		refreshQuestions();
		setPollName(p.getTitle());
	}
}