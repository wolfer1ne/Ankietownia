package server;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.List;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.google.gson.Gson;

import data.Poll;

import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ListClientsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JList<String> list;
	DefaultListModel<String> clientList;
	ArrayList<String> connectedClients;
	java.util.List<Runnable> tasks;
	Poll p;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ListClientsDialog dialog = new ListClientsDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ListClientsDialog() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		clientList = new DefaultListModel<String>();
		list = new JList<String>(clientList);
		contentPanel.add(list, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnSend = new JButton("Wyślij");
				btnSend.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						sendPolls();
					}
				});
				btnSend.setActionCommand("OK");
				buttonPane.add(btnSend);
				getRootPane().setDefaultButton(btnSend);
			}
			{
				JButton btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}
	public ListClientsDialog(Poll p, ArrayList<String> clients, java.util.List<Runnable> tasks) {
		this();
		this.p = p;
		connectedClients = clients;
		this.tasks = tasks;
		moveClients();
	}
	
	void moveClients() {
		for (String x : connectedClients) {
			clientList.addElement(x);
		}
	}
		
	void sendPolls() {
		java.util.List<String> selectedIps = list.getSelectedValuesList();
		Gson gson = new Gson();
		String JSONString = gson.toJson(p);
		System.out.println(JSONString);
		for (String ip : selectedIps) {
			for (Runnable r : tasks) {
				if (((ClientHandler) r).getClientIP().equals(ip)) {
					((ClientHandler) r).send(JSONString);
				}
			}
		}
	}
}