package server;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import java.awt.TextArea;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import data.Poll;
import storage.StorageAdmin;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// Okno głowne
public class AdminWindow {

	private JFrame frmAdmin;
	JList<String> PollList; // Lista z nazwami ankiet
	DefaultListModel<String> listModel;
	TextArea PollInfoText; // Nieuzywane
	Poll currentPoll; // Obecnie zaznaczona ankieta
	private JLabel lblStatLabel; // Etykieta z informacjami
	ServerClass server = null;
	Thread serverThread = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminWindow window = new AdminWindow();
					window.frmAdmin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * Start server
	 */
	public AdminWindow() {
		initialize();
		server = new ServerClass(32000);
		serverThread = new Thread(server);
		serverThread.start();
		setStatTxt(server.getServerInfo());
	}
	
	void setStatTxt(String txt) {
		lblStatLabel.setText(txt);
	}
	
	// Otworz okno z tworzeniem ankiety
	void add() {
		new AddOrModifyPollFrame().setVisible(true);
	}
	
	// Jesli ankieta jest zaznaczona
	// przejdz do edycji
	void edit() {
		int index = PollList.getSelectedIndex();
		if (index != -1) {
			String name = (String)PollList.getSelectedValue();
			new AddOrModifyPollFrame(name).setVisible(true);
		}
	}
	
	// Odswiez liste ankiet
	void refresh() {
		listModel.clear();
		PollInfoText.setText("");
		List<String> l = StorageAdmin.getServerPollsNames();
		
		for (String x : l) {
			listModel.addElement(x);
		}	
	}
	
	void sendPolls() {
		int index = PollList.getSelectedIndex();
		if (index == -1) return;
		
		String pollName = (String)PollList.getSelectedValue();
		Poll p = StorageAdmin.getServerPoll(pollName);
		
		ArrayList<String> clients = server.getClients();
		List<Runnable> tasks = server.getTasks();
		new ListClientsDialog(p, clients, tasks).setVisible(true);
	}
	
	// Usun ankiete
	private void remove() {
		int index = PollList.getSelectedIndex();
		if (index == -1) {
			return;
		}
		
		String name = (String)PollList.getSelectedValue();
		StorageAdmin.removeServerPoll(name);
		listModel.remove(index);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAdmin = new JFrame();
		frmAdmin.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				server.stop();
			}
		});
		frmAdmin.setTitle("Panel administracyjny");
		frmAdmin.setBounds(100, 100, 642, 475);
		frmAdmin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAdmin.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 52, 298, 374);
		frmAdmin.getContentPane().add(scrollPane);
		
		listModel = new DefaultListModel<String>();
		PollList = new JList<String>(listModel);
		scrollPane.setViewportView(PollList);
		PollList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			
		lblStatLabel = new JLabel("Tworzenie ankiet - Wersja testowa");
		lblStatLabel.setBounds(10, 11, 606, 27);
		frmAdmin.getContentPane().add(lblStatLabel);
		
		PollInfoText = new TextArea();
		PollInfoText.setEnabled(false);
		PollInfoText.setEditable(false);
		PollInfoText.setBounds(314, 52, 302, 198);
		frmAdmin.getContentPane().add(PollInfoText);
		
		JButton btnSendPoll = new JButton("Wyślij");
		btnSendPoll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendPolls();
			}
		});
		btnSendPoll.setBounds(527, 279, 89, 92);
		frmAdmin.getContentPane().add(btnSendPoll);
		
		JButton btnAddPoll = new JButton("Dodaj");
		btnAddPoll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				add();
			}
		});
		btnAddPoll.setBounds(318, 256, 89, 48);
		frmAdmin.getContentPane().add(btnAddPoll);
		
		JButton btnEditPoll = new JButton("Edytuj");
		btnEditPoll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				edit();
			}
		});
		btnEditPoll.setBounds(318, 308, 89, 48);
		frmAdmin.getContentPane().add(btnEditPoll);
		
		JButton btnDeletePoll = new JButton("Usuń");
		btnDeletePoll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				remove();
			}
		});
		btnDeletePoll.setBounds(318, 359, 89, 48);
		frmAdmin.getContentPane().add(btnDeletePoll);
		
		JButton btnRefreshContent = new JButton("Odśwież");
		btnRefreshContent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refresh();
			}
		});
		btnRefreshContent.setBounds(417, 294, 89, 62);
		frmAdmin.getContentPane().add(btnRefreshContent);
		
		refresh();
	}
}