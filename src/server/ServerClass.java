package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.gson.internal.bind.util.ISO8601Utils;

public class ServerClass implements Runnable {
	// https://www.richardnichols.net/2012/01/how-to-get-the-running-tasks-for-a-java-executor/
	private LinkedBlockingQueue<Runnable> taskQueue;
	private List<Runnable> running;
	
	private static final int NUM_THREADS = 16;
	
	ThreadPoolExecutor executor;
	
	private boolean isRunning = true;
	private int serverPort = 0;
	private ServerSocket serverSocket;
	private InetAddress serverAddress;
	
	public ServerClass(int port) {
		serverPort = port;
		try {
			serverAddress = getLocalHostLANAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		taskQueue = new LinkedBlockingQueue<Runnable>();
		running = Collections.synchronizedList(new ArrayList<Runnable>());
		
		executor = 
		        new ThreadPoolExecutor(NUM_THREADS, NUM_THREADS,
		            0L, TimeUnit.MILLISECONDS,
		            taskQueue,
		            Executors.defaultThreadFactory()) 
		    {

		        @Override
		        protected <T> FutureTask<T> newTaskFor(final Runnable runnable, T value) {
		            return new FutureTask<T>(runnable, value) {
		                @Override
		                public String toString() {
		                    return runnable.toString();
		                }
		            };
		        }

		        @Override
		        protected void beforeExecute(Thread t, Runnable r) {
		            super.beforeExecute(t, r);
		            running.add(r);
		        }

		        @Override
		        protected void afterExecute(Runnable r, Throwable t) {
		            super.afterExecute(r, t);
		            running.remove(r);
		        }
		    };
	}
	
	public List<Runnable> getTasks() {
		return running;
	}
	public ArrayList<String> getClients() {
		System.out.println("getCLients");
		ArrayList<String> ret = new ArrayList<String>();
		for (Runnable x : running) {
			System.out.println(((ClientHandler) x).getClientIP());
			ret.add(((ClientHandler) x).getClientIP());
		}
		return ret;
	}
	public int getServerPort() { return serverPort; }
	public void startServer() throws IOException {
			serverSocket =
					new ServerSocket(serverPort, 0, serverAddress);
	}
	
	public synchronized boolean isRunning() {
		return this.isRunning;
	}
	public synchronized void stop() {
		isRunning = false;
		executor.shutdown();
		try {
			if (serverSocket != null) {
				serverSocket.close();
				serverSocket = null;
				taskQueue = null;
				running = null;
			}
		} catch (Exception e) {
			serverSocket = null;
			taskQueue = null;
			running = null;
		}
	}
	
	// http://stackoverflow.com/a/20418809
	public static InetAddress getLocalHostLANAddress() throws UnknownHostException {
	    try {
	        InetAddress candidateAddress = null;
	        // Iterate all NICs (network interface cards)...
	        for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
	            NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
	            // Iterate all IP addresses assigned to each card...
	            for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements();) {
	                InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
	                if (!inetAddr.isLoopbackAddress()) {
	                    if (inetAddr.isSiteLocalAddress()) {
	                        // Found non-loopback site-local address. Return it immediately...
	                        return inetAddr;
	                    }
	                    else if (candidateAddress == null) {
	                        // Found non-loopback address, but not necessarily site-local.
	                        // Store it as a candidate to be returned if site-local address is not subsequently found...
	                        candidateAddress = inetAddr;
	                        // Note that we don't repeatedly assign non-loopback non-site-local addresses as candidates,
	                        // only the first. For subsequent iterations, candidate will be non-null.
	                    }
	                }
	            }
	        }
	        if (candidateAddress != null) {
	            // We did not find a site-local address, but we found some other non-loopback address.
	            // Server might have a non-site-local address assigned to its NIC (or it might be running
	            // IPv6 which deprecates the "site-local" concept).
	            // Return this non-loopback candidate address...
	            return candidateAddress;
	        }
	        // At this point, we did not find a non-loopback address.
	        // Fall back to returning whatever InetAddress.getLocalHost() returns...
	        InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
	        if (jdkSuppliedAddress == null) {
	            throw new UnknownHostException("The JDK InetAddress.getLocalHost() method unexpectedly returned null.");
	        }
	        return jdkSuppliedAddress;
	    }
	    catch (Exception e) {
	        UnknownHostException unknownHostException = new UnknownHostException("Failed to determine LAN address: " + e);
	        unknownHostException.initCause(e);
	        throw unknownHostException;
	    }
	}

	public static String getLocalIP() {
		try {
			return getLocalHostLANAddress().toString().substring(1);
		} catch (UnknownHostException e) {
			return "Unknown local IP";
		}
	}
	public static String getExternalIp() throws Exception {
	        URL whatismyip = new URL("http://checkip.amazonaws.com");
	        BufferedReader in = null;
	        try {
	            in = new BufferedReader(new InputStreamReader(
	                    whatismyip.openStream()));
	            String ip = in.readLine();
	            return ip;
	        } finally {
	            if (in != null) {
	                try {
	                    in.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	public String getServerInfo() {
		String local = getLocalIP();
		String ext = null;
		try {
			ext = getExternalIp();
		} catch (Exception e) {
			ext = "Unknown External IP";
		}
		
		return "(" + local + " / " + ext + "):" + getServerPort();
	}

	public void run() {
		try {
			startServer();
			System.out.println("Serwer nasłuchuje");
            while (!Thread.currentThread().isInterrupted() && isRunning()) {
            	Socket clientSocket = null;
                try {
                	clientSocket = serverSocket.accept();
                } catch (IOException e) {
	                if (!isRunning()) {
	                	System.out.println("Serwer zatrzymany");
	                	break;
	                }
                }
                
                executor.execute(new ClientHandler(clientSocket));
            }
        } catch (IOException e) {
			e.printStackTrace();
        }
		System.out.println("Serwer zatrzymany");
		executor.shutdown();
	}
}