package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ClientTest {
	Socket socket;
	BufferedReader reader = null;
	BufferedWriter writer = null;
	String ip;
	int port;
	
	ClientTest() {}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		ClientTest client = new ClientTest();
		client.ip = scanner.nextLine();
		client.port = scanner.nextInt();
		
		try {
			client.socket = new Socket(client.ip, client.port);
		} catch (UnknownHostException e) {} 
		catch (IOException e) {}
		
		try {
			client.writer = new BufferedWriter(new OutputStreamWriter(client.socket.getOutputStream(), StandardCharsets.UTF_8));
			client.reader = new BufferedReader(new InputStreamReader(client.socket.getInputStream()));
		} catch (Exception e) {}
		
		try {
			client.writer.write("KLIENT");
			client.writer.write("KLIENT");
			client.writer.write("KLIENT");
			client.writer.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		scanner.nextLine();
		try {
			client.socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
