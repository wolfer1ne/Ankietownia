package data;

public final class QuestionMulti extends Question {
	public QuestionMulti() {
		super();
	}

	public QuestionMulti(String title, String[] args) {
		super(title, args);
		this.type = QuestionType.MULTIPLE;
	}
}