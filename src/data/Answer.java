package data;

/* Klasa pojedynczego wpisu odpowiedzi
 * 
 */
public class Answer {
	// Treść odpowiedzi
	String  content;

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	// Ukryte ID odpowiedzi
	Integer ID;
	
	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Answer() {
		this.content = "UNINITIALIZED";
		this.ID = -1;
	}
	
	public Answer(String content) {
		this.content = content;
	}
	
	public String toString() {
		return "ANSWER " + ID + ": " + content;
	}
}