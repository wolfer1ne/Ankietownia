package data;

public final class QuestionSingle extends Question {
	public QuestionSingle() {
		super();
	}

	public QuestionSingle(String title, String[] args) {
		super(title, args);
		this.type = QuestionType.SINGLE;
	}
}