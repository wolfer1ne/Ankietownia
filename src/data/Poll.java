package data;

import java.util.ArrayList;
import java.util.List;

/* Klasa ankiety
 * 
 */
public class Poll {
	String title; // Tytuł ankiety
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	// Lista z pytaniami
	List <Question> questions;
	
	public Poll() {
		this.title = "UNINITIALIZED";
		this.questions = new ArrayList<Question>();
	}

	public Poll(String title) {
		this();
		this.title = title;
	}
	
	// Przelicz ID pytań
	public void recalcIDs() {
		int size = questions.size();
		for (int i = 0; i < size; i++) {
			questions.get(i).ID = i;
			questions.get(i).recalcAnswersIDs(); // i odpowiedzi
		}
	}
	
	public void addQuestion(Question q) {
		questions.add(q);
		questions.get(questions.size() - 1).ID =
				questions.size();
	}
	
	public void removeQuestion(int index) {
		questions.remove(index);
	}
	
	public List<Question> getQuestions() {
		return this.questions;
	}
	
	public Question getQuestionAtIndex(int index) {
		return this.questions.get(index);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ANKIETA - " + title + System.lineSeparator());
		
		for (Question x : questions) {
			sb.append("\t" + x.toString() + System.lineSeparator());
		}
		
		sb.append(System.lineSeparator());
		
		return sb.toString();
	}
}