package data;

import java.util.ArrayList;
import java.util.List;

/* Klasa pojedynczego pytania
 * 
 */
public class Question {

	protected String content; // Treść

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	// Ukryte ID pytania
	protected Integer ID; 

	public Integer getID() {
		return ID;
	}

	// Typ pytania
	public enum QuestionType { UNKNOWN, SINGLE, MULTIPLE };
	protected QuestionType type;

	public QuestionType getType() {
		return type;
	}

	public void setType(QuestionType type) {
		this.type = type;
	}

	// Lista odpowiedzi
	protected List <Answer> answers; 
	
	public List<Answer> getAnswers() {
		return this.answers;
	}
	
	public Answer getAnswer(int index) {
		return this.answers.get(index);
	}
	
	public void deleteAnswer(int index) {
		answers.remove(index);
	}
	
	// Przelicz indeksy odpowiedzi
	public void recalcAnswersIDs() {
		int size = answers.size();
		
		for (int i = 0; i < size; i++) {
			answers.get(i).setID(i);
		}
	}

	public Question() {
		this.content = "NOT INITIALIZED";
		this.ID = -1;
		this.type = QuestionType.UNKNOWN;
		this.answers = new ArrayList<Answer>();
	}
	
	Question(String title, String[] args) {
		this();
		this.content = title;
			
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				Answer element = new Answer(args[i]);
				element.ID = i + 1;
				this.answers.add(element);
			}
		}
	}

	public static String typeToString(QuestionType type) {
		switch (type) {
		case MULTIPLE:
			return "Multiple";
		case SINGLE:
			return "Single";
		default:
			return "";
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question " + this.ID + ": " + this.content
				+ System.lineSeparator());
		
		for (Answer x : answers) {
			sb.append("\t" + x.toString() + System.lineSeparator());
		}
		
		sb.append(System.lineSeparator());
		
		return sb.toString();
	}
}