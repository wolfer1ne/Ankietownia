package data;

import java.util.ArrayList;
import java.util.List;

/* Klasa zarządzająca wpisami odpowiedzi
 * 
 */
public class ClientResponse {
	String pollName; // Nazwa ankiety
	String identificatorName; // identyfikacja
	List<ClientQuestionResponse> questionResponses; // Lista wpisów
	
	public ClientResponse() {
		pollName = "UNINITIALIZED";
		identificatorName = "UNINITIALIZED";
		questionResponses = new ArrayList<ClientQuestionResponse>();
	}

	public ClientQuestionResponse getQuestionResponse(int index) {
		try {
			return this.questionResponses.get(index);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	public void addQuestionResponse(ClientQuestionResponse element) {
		questionResponses.add(element);
	}
}