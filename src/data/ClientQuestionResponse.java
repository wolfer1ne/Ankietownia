package data;

import java.util.ArrayList;
import java.util.List;

import data.Question.QuestionType;

/* Klasa pojedynczego wpisu odpowiedzi
 */
public class ClientQuestionResponse {
	Integer questionID;
	QuestionType questionType;
	List <Integer> questionAnswers;
	
	public ClientQuestionResponse() {
		questionID = -1;
		questionType = QuestionType.UNKNOWN;
		questionAnswers = new ArrayList<Integer>();
	}
	
	public List<Integer> getQuestionAnswers() {
		return questionAnswers;
	}

	public Integer getQuestionID() {
		return questionID;
	}

	public void setQuestionID(Integer questionID) {
		this.questionID = questionID;
	}

	public void setSingleType() {
		questionType = QuestionType.SINGLE;
	}
	
	public void setMultiType() {
		questionType = QuestionType.MULTIPLE;
	}

	// Pojedyncza odpowiedz
	public Integer getSingleAnswer() {
		return getSingleAnswer(0);
	}
	
	public Integer getSingleAnswer(int i) {
		try {
			return questionAnswers.get(i);
		} catch (Exception e) {
			return -1;
		}
	}
	
	// Dodaje pojedynczą odpowiedz
	// Jesli istnieje, zmienia wartosc
	public void setSingleAnswer(Integer i) {
		if (getSingleAnswer() == -1) {
			questionAnswers.add(i);
		} else {
			questionAnswers.set(0, i);
		}
	}
	
	public boolean isSetMultiAnswer(Integer i) {
		for (Integer x : questionAnswers) {
			if (x == i) return true;
		}
		return false;
	}
	
	public void setMultiAnswer(Integer i) {
		for (Integer x : questionAnswers) {
			if (x == i) return;
		}
		questionAnswers.add(i);
	}
	
	public void unsetMultiAnswer(Integer s) {
		for (int i = 0; i < questionAnswers.size(); i++) {
			if (questionAnswers.get(i) == s) questionAnswers.remove(i);
		}
	}
}