package tests;

import data.Poll;	
import data.QuestionMulti;
import data.QuestionSingle;

public class StorageSaveTest {

	public static void main(String[] args) {
		Poll pl = new Poll("Testowa Ankieta");
		QuestionSingle single = new QuestionSingle("Pojedycze", new String[] {
				"Jeden", "Dwa", "Trzy"} );
		pl.addQuestion(single);
		QuestionMulti multi = new QuestionMulti("Wielokrotne",
				new String[] { "1", "2", "3" });
		pl.addQuestion(multi);
		
		storage.StorageAdmin.savePoll(pl);
		System.out.println(pl.toString());
	}

}