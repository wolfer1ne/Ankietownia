package tests;

import data.Poll;	
import data.QuestionMulti;
import data.QuestionSingle;
import storage.StorageAdmin;

public final class TestPoll {

	public static void main(String[] args) {
		Poll pl = new Poll("Dwa pytania");
		
		QuestionSingle single = new QuestionSingle("Czy jesteś studentem?",
				new String[] {
				"Tak", "Nie", "Nie wiem"
				});
		pl.addQuestion(single);
		
		QuestionMulti multi = new QuestionMulti("Ukończone szkoły",
				new String[] { "Podstawowa", "Gimnazjum", "Liceum", "Technikum" });
		pl.addQuestion(multi);
		
		//System.out.print(pl.toString());
		StorageAdmin.savePoll(pl);
	}

}