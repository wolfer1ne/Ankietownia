package tests;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import java.awt.Insets;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.CardLayout;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class JDialogTest extends JDialog {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			JDialogTest dialog = new JDialogTest();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public JDialogTest() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblTest = new JLabel("test");
		lblTest.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTest.setBounds(10, 11, 414, 36);
		getContentPane().add(lblTest);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 58, 290, 193);
		getContentPane().add(scrollPane);
		
		JPanel pnlContent = new JPanel();
		scrollPane.setViewportView(pnlContent);
		pnlContent.setLayout(new GridLayout(0, 2, 0, 0));
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("New check box");
		pnlContent.add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxNewCheckBox_4 = new JCheckBox("New check box");
		pnlContent.add(chckbxNewCheckBox_4);
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("New check box");
		pnlContent.add(chckbxNewCheckBox_2);
		
		JCheckBox chckbxNewCheckBox_3 = new JCheckBox("New check box");
		pnlContent.add(chckbxNewCheckBox_3);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("New check box");
		pnlContent.add(chckbxNewCheckBox);
		
		JPanel pnlButton = new JPanel();
		pnlButton.setBounds(309, 58, 115, 193);
		getContentPane().add(pnlButton);
		pnlButton.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnNewButton_1 = new JButton("New button");
		pnlButton.add(btnNewButton_1);
		
		JButton btnNewButton = new JButton("New button");
		pnlButton.add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("New button");
		pnlButton.add(btnNewButton_2);
	}
}
