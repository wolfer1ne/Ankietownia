package client;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import storage.StorageAdmin;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.JTextField;

/*
 * Główne okno klienta
 */
@SuppressWarnings("serial")
public class ClientFrm extends JFrame {

	private JPanel contentPane;
	DefaultListModel<String> listModel;
	private JList<String> list;
	private JLabel lblStatus;
	ClientRunner client = null;
	Thread clientThread = null;
	boolean connected = false;
	private JTextField txtIP;
	private JTextField txtPort;
	String serverIP = null;

	synchronized boolean isConnected() { return connected; }
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientFrm frame = new ClientFrm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/* Zwraca indeks aktualnie zaznaczonej ankiety
	 * Jeśli żadna nie jest wybrana zwraca -1
	 */
	int getSelectedPollIndex() {
		int index = list.getSelectedIndex();
		if (index == -1) return -1;
		return index;
	}
	
	/* Zwraca nazwę aktualnie zaznaczonej ankiety
	 * Przy braku zaznaczenia zwraca null
	 */
	String getSelectedPollName() {
		int index = getSelectedPollIndex();
		if (index == -1) return null;
		
		return (String) list.getSelectedValue();
	}
	
	/*
	 * Jeśli jest zaznaczona ankieta, przenosi w okno wypełniania
	 * W przypadku braku zaznaczenia ankiety nie dzieje się nic
	 */
	void fillPoll() {
		if (!isConnected()) return;
		int index = getSelectedPollIndex();
		if (index == -1) return;
		
		String name = getSelectedPollName();
		
		JDialog dialog = new PollFillDialog(name, client);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	
	/*
	 * Usuwa aktualnie zaznaczoną ankietę z lokalnej puli
	 * Brak zaznaczenia - brak akcji
	 */
	void cancelPoll() {
		int index = getSelectedPollIndex();
		if (index == -1) return;
		
		String name = getSelectedPollName();
		
		StorageAdmin.removeClientPoll(name);
		listModel.remove(index);
	}
	
	void connectToServer() {
		client =
				new ClientRunner(txtIP.getText(),
				Integer.parseInt(txtPort.getText()));
		
		try {
			client.tryConnect();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clientThread = new Thread(client);
		clientThread.start();
		serverIP = client.getServerIP();

		lockAddressFields();
		connected = true;
	}
	
	void disconnectFromServer() {
		client.stop();		
		resetStatusLabel();
		resetAddressFields();
		unlockAddressFields();
		connected = false;
	}
	
	/* Ustawia domyślną wartość etykiety statusu
	 */
	void resetStatusLabel() {
		setStatusLabel("Nie połączono z serwerem ankiet");
	}
	
	void resetAddressFields() {
		txtIP.setText("");
		txtPort.setText("");
	}
	
	void lockAddressFields() {
		txtIP.setEnabled(false);
		txtPort.setEnabled(false);
	}
	
	void unlockAddressFields() {
		txtIP.setEnabled(true);
		txtPort.setEnabled(true);
	}
	
	/* Ustawia wartość etykiety statusu
	 * 
	 */
	public void setStatusLabel(final String n) {
		if (EventQueue.isDispatchThread()) {
			lblStatus.setText(n);
		} else {
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
						lblStatus.setText(n);
				}
			});
		}
	}
	
	/* Odświerza listę ankiet z lokalnej puli
	 * 
	 */
	private void refreshPolls() {
		listModel.clear();
		List<String> l = StorageAdmin.getClientPollsNames();
		
		for (String x : l) listModel.addElement(x);	
	}

	/**
	 * Create the frame.
	 */
	public ClientFrm() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (client == null) return;
				client.stop();
				client = null;
			}
		});
		setTitle("Klient Ankiet");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 559, 307);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblStatus = new JLabel("Nie połączono z serwerem ankiet");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblStatus.setBounds(10, 11, 296, 30);
		contentPane.add(lblStatus);
		
		listModel = new DefaultListModel<String>();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 65, 315, 186);
		contentPane.add(scrollPane);
		list = new JList<String>(listModel);
		scrollPane.setViewportView(list);
		list.setToolTipText("Lista ankiet, które można wypełnić (i przesłać odpowiedzi do serwera) lub odrzucić");
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JButton btnAccept = new JButton("Wypełnij");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fillPoll();
			}
		});
		btnAccept.setBounds(335, 65, 89, 55);
		contentPane.add(btnAccept);
		
		JButton btnCancel = new JButton("Odrzuć");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				cancelPoll();
			}
		});
		btnCancel.setBounds(335, 196, 89, 55);
		contentPane.add(btnCancel);
		
		JButton btnConnect = new JButton("Połącz");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (isConnected()) return;
				connectToServer();
				setStatusLabel("Połączono z " + serverIP);
			}
		});
		btnConnect.setBounds(444, 65, 89, 36);
		contentPane.add(btnConnect);
		
		JButton btnDisconnect = new JButton("Rozłącz");
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!isConnected()) return;
				disconnectFromServer();
			}
		});
		btnDisconnect.setBounds(444, 121, 89, 36);
		contentPane.add(btnDisconnect);
		
		txtIP = new JTextField();
		txtIP.setBounds(293, 18, 131, 20);
		contentPane.add(txtIP);
		txtIP.setColumns(10);
		
		txtPort = new JTextField();
		txtPort.setBounds(444, 18, 86, 20);
		contentPane.add(txtPort);
		txtPort.setColumns(10);
		
		JButton btnRefresh = new JButton("Odśwież");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refreshPolls();
			}
		});
		btnRefresh.setBounds(335, 131, 89, 54);
		contentPane.add(btnRefresh);
		
		refreshPolls();
	}
}