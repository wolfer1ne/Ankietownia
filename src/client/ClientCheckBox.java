package client;

import javax.swing.JCheckBox;

/*
 * Rozszerzona klasa JCheckBox
 * Zawiera ID pytania
 */
@SuppressWarnings("serial")
public class ClientCheckBox extends JCheckBox {
	Integer ID; // ID pytania, które CheckBox obsługuje
	
	public ClientCheckBox() {
		super();
		this.ID = -1;
	}

	public ClientCheckBox(String name, int iD) {
		super(name);
		this.ID = iD;
	}
	
	public Integer getCheckBoxID() {
		return this.ID;
	}
}