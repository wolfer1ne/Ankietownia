package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import data.Answer;
import data.Question;
import storage.StorageClient;

/* Klasa do zarządzania obsługi panelem treści
 * Panel treści - panel z odpowiedziami
 */
public class ClientPanelContentManager {
	private Question q = null; // Aktualne pytanie
	private ButtonGroup btnGrp = null;
	/* Przycisk dodawany na sam koniec, aby wysłac odpowiedzi
	 * 
	 */
	private JButton sendBtn = new JButton("Wyślij");
	/* Lista z checkboxami (Wielokrotne odpowiedzi)
	 * 
	 */
	private List <ClientCheckBox> chkBoxes;
	/* Lista z radiobuttonami (Pojedyncze odpowiedzi)
	 * Dodawane są do btnGrp (ButtonGroup na górze)
	 */
	private List <ClientRadioButton> rdBtns;
	private JPanel p = null; // Panel z odpowiedziami
	/* Klasa zarządzającama odpowiedziami
	 * 
	 */
	private ClientResponseManager crm = null;
	ClientRunner cl;
	
	public ClientPanelContentManager() {
		chkBoxes = new ArrayList<ClientCheckBox>();
		rdBtns = new ArrayList<ClientRadioButton>();
	}
	
	public void setPanel(JPanel p) {
		this.p = p;
	}
	
	public void setClientRunner(ClientRunner c) {
		this.cl = c;
	}
	
	public void setClientResponseManager(ClientResponseManager c) {
		this.crm = c;
	}

	public void resetPanel() {
		p.removeAll();
	}
	
	public void repaintPanel() {
		p.repaint();
	}
	
	void clearElements() {
		chkBoxes.clear();
		rdBtns.clear();
		btnGrp.clearSelection();
	}

	public void setQuestion(Question a) {
		this.q = a;
	}
	/* Tworzy odpowiedzi
	 * 
	 */
	private void generateElements() {
		clearElements(); // Usunięcie aktualnych danych

		switch (q.getType()) {
		case MULTIPLE: // Tworzenie pytań wielokrotnych
			generateCheckBoxes();
			break;
		case SINGLE: // Tworzenie pytań pojedynczych
			generateRadioButtons();
			addRadioButtonsToGroup();
			break;
		case UNKNOWN: break;
		}
	}
	
	private void addCheckBoxesToPanel() {
		for (ClientCheckBox chbox : chkBoxes) {
			p.add(chbox);
		}
	}
	
	private void addRadioButtonsToPanel() {
		for (ClientRadioButton btn : rdBtns) {
			p.add(btn);
		}
	}

	private void addElementsToPanel() {
		switch(q.getType()) {
		case MULTIPLE: addCheckBoxesToPanel(); break;
		case SINGLE: addRadioButtonsToPanel(); break;
		case UNKNOWN: break;
		}
	}

	public void setRadioBtnGroup(ButtonGroup btnGrp) {
		this.btnGrp = btnGrp;	
	}

	private void generateCheckBoxes() {
		for (Answer a : q.getAnswers()) {
			chkBoxes.add(new ClientCheckBox(a.getContent(),
					a.getID()));
		}		
	}
	
	private void addRadioButtonsToGroup() {
		for (ClientRadioButton btn : rdBtns) {
			btnGrp.add(btn);
		}
	}
	
	private void generateRadioButtons() {
		for (Answer a : q.getAnswers()) {
			rdBtns.add(new ClientRadioButton(a.getContent(),
					a.getID()));
		}		
	}
	/* Generuje elementy i przenosi je na panel */
	public void transformAnswersToPanel() {
		resetPanel(); // Wyczyść panel
		generateElements(); // Wytwórz elementy
		addElementsToPanel(); // Dodaj do panelu
		repaintPanel(); // Odśwież panel
	}
	
	/*
	 * Zachowanie odpowiedzi
	 */
	public void storeUserSelectedAnswers(Question q) {
		int qID = q.getID(); // Pobierz ID pytania
		// Pobierz odpowiedzi do aktualnego pytania
		ClientResponse cr = crm.getResponseEntry(qID); 
		if (cr == null) { // Jeśli ich nie ma
			cr = new ClientResponse(); // Zrób nowy wpis
			cr.setQuestionID(qID); // Ustaw ID pytania
			cr.setQuestionType(q.getType()); // Ustaw typ pytania

			crm.getClientResponses().add(cr); // Dodaj wpis
		}
		
		switch(q.getType()) {
		case SINGLE: // Zachowaj pojedyncze odpowiedz (RadioButton)
			storeSingleAnswer(cr);
			break;
		case MULTIPLE: // Zachowaj wielokrotne odpowiedzi (CheckBox)
			storeMultipleAnswers(cr);
			break;
		case UNKNOWN: break;
		}
	}
	
	/* Pojedyncza odpowiedz
	 * Pętla sprawdza, który radiobutton jest zaznaczony
	 * Zaznaczony jest zapisywany
	 */
	private void storeSingleAnswer(ClientResponse c) {	
		for (ClientRadioButton crb : rdBtns) {
			if (crb.isSelected()) {
				c.storeSingleEntry(crb.getRadioButtonID());
			}
		}			
	}
	
	/* Wielokrotna odpowiedz
	 * Pętla sprawdza, które CheckBoxy są zaznaczone
	 * Zaznaczone elementy są zapisywane
	 */
	private void storeMultipleAnswers(ClientResponse c) {
		c.resetSelectedAnswers();
		for (ClientCheckBox ccb : chkBoxes) {
			if (ccb.isSelected()) {
				c.storeMultiEntry(ccb.getCheckBoxID());
			}
		}	
	}
	
	/*
	 * Przywróć odpowiedzi na dane pytanie q
	 */
	public void restoreUserSelectedAnswers(Question q) {
		int qID = q.getID(); // Pobierz ID
		ClientResponse cr = crm.getResponseEntry(qID); // Pobierz wpis
		if (cr == null) { // Jeśli nie ma nic, nic nie rób
			return;
		}
		
		switch(q.getType()) {
		case SINGLE: // Przywróć pojedynczą odpowiedz
			restoreSingleAnswer(cr);
			break;
		case MULTIPLE: // Przywróć wielokrotne odpowiedzi
			restoreMultiAnswers(cr);
			break;
		case UNKNOWN: break;
		}
	}
	
	/* 
	 * Zaznacza dany RadioButton (po odpowiednim ID)
	 */
	private void restoreSingleAnswer(ClientResponse c) {
		int selectedAnswer = c.getSingleEntry();
		for (ClientRadioButton crb : rdBtns) {
			if (crb.getRadioButtonID() == selectedAnswer) {
				crb.setSelected(true);
			}
		}
	}
	
	/*
	 * Zaznacza dane CheckBoxy (po odpowiednich ID)
	 */
	private void restoreMultiAnswers(ClientResponse c) {
		List <Integer> answers = c.getMultiEntry();
		if (answers == null) return;

		for (Integer x : answers) {
			for (ClientCheckBox ccb : chkBoxes) {
				if (ccb.getCheckBoxID().equals(x)) {
					ccb.setSelected(true);
				}
			}
		}
	}

	/*
	 * Dodaj przycisk wyślij do panelu
	 */
	public void addSendButton() {
		resetPanel();
		sendBtn.setSize(150, 50);
		p.add(sendBtn);
		repaintPanel();
		sendBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				crm.setResponseName();
				Gson gson = new Gson();
				String JSONString = gson.toJson(crm);
				cl.send(JSONString);
			}
		});
	}
}