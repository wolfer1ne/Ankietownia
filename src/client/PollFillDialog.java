package client;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import data.Poll;
import data.Question;
import storage.StorageAdmin;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollPane;

/* Okno wypełniania ankiety
 */
@SuppressWarnings("serial")
public class PollFillDialog extends JDialog {
	private Poll p; // Aktualna ankieta
	private Question currentQuestion = null; // Aktualne pytanie
	private int currentQuestionNumber = 0; // Numer pytania
	private int questionLimit; // Limit pytań
	private JLabel lblText; // Etykieta z treścią pytania
	private JPanel pnlContent; // Panel z odpowiedziami
	ClientPanelContentManager cpcm = null; // Zarządzanie odpowiedziami
	ClientResponseManager crm = null; // Zarządzanie odpowiedziami
	private final ButtonGroup btnGrp = new ButtonGroup();
	private JButton btnNext;
	private JButton btnPrev;
	ClientRunner client;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PollFillDialog dialog = new PollFillDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	void init(String name) {
		p = StorageAdmin.getClientPoll(name);
		setWindowName(name);
	}
	
	void setWindowName(String name) {
		String txt = "Ankieta - " + name;
		setTitle(txt);
	}
	
	void cancelFill() {
		int reply = JOptionPane.showConfirmDialog(null, 
				"Zakończyć wypełnianie ankiety?",
				"Koniec?",
				JOptionPane.YES_NO_OPTION);

		if (reply == JOptionPane.YES_OPTION) {
			dispose();
		}	
	}
	
	/*
	 * (Numer pytania/Limit pytań). Treść pytania
	 */
	void updateQuestionInfo() {
		String val =
				"(" + 
				String.valueOf(currentQuestion.getID()) + 
				"/" +
				(questionLimit + 1) +
				") - " +
				currentQuestion.getContent();

		lblText.setText(val);	
	}
	
	// Załaduj pytanie z ankiety
	void loadQuestion(int number) {
		currentQuestion = p.getQuestionAtIndex(number);
	}
	
	// Zmniejsz bezpiecznie numer pytania
	boolean decQuestionIndex() {
		if (currentQuestionNumber <= 0) { 
			currentQuestionNumber = 0;
			return false;
		}
		
		currentQuestionNumber -= 1;
		return true;
	}
	
	// Zmniejsz bezpiecznie numer pytania
	// Jeśli jest zmiana zwraca true
	boolean incQuestionIndex() {
		if (currentQuestionNumber >= questionLimit) {
			currentQuestionNumber = questionLimit;
			return false;
		}
		
		currentQuestionNumber += 1;
		return true;
	}
	
	// Zwiększ bezpiecznie numer pytania
	// Jeśli jest zmiana zwraca true
	void nextQuestion() {
		if (incQuestionIndex()) {
			updateQuestion();
		} else { // Nie ma już pytań
			cpcm.addSendButton(); // Dodaj przycisk "Wyślij"
			btnNext.setEnabled(false);
		}
	}
	
	// Poprzednie pytanie
	void prevQuestion() {
		if (decQuestionIndex()) { // Zmniejdz indeks
			updateQuestion(); // Uaktualnij pytanie
			btnNext.setEnabled(true); // Uaktywnij przycisk
		}
	}
	
	void updateQuestion() {
		// Zapis odpowiedzi
		cpcm.storeUserSelectedAnswers(currentQuestion);
		
		// Załaduj pytanie
		loadQuestion(currentQuestionNumber);
		showCurrentQuestion(); // Pokaż je

		// Przywróć zaznaczone odpowiedzi
		cpcm.restoreUserSelectedAnswers(currentQuestion);
	}
	
	void showCurrentQuestion() {
		updateQuestionInfo();
		cpcm.setQuestion(currentQuestion);
		cpcm.transformAnswersToPanel();
	}

	private PollFillDialog() {
		setTitle("Ankieta - ");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		{
			lblText = new JLabel("TEXT");
			lblText.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lblText.setBounds(10, 11, 414, 40);
			getContentPane().add(lblText);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 61, 289, 190);
			getContentPane().add(scrollPane);
			{
				pnlContent = new JPanel();
				scrollPane.setViewportView(pnlContent);
				pnlContent.setLayout(new GridLayout(0, 2, 0, 0));
			}
		}
		{
			JPanel pnlButtons = new JPanel();
			pnlButtons.setBounds(309, 59, 115, 192);
			getContentPane().add(pnlButtons);
			pnlButtons.setLayout(new GridLayout(0, 1, 0, 0));
			{
				btnNext = new JButton("NASTĘPNE");
				btnNext.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						nextQuestion();
					}
				});
				pnlButtons.add(btnNext);
			}
			{
				btnPrev = new JButton("POPRZEDNIE");
				btnPrev.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						prevQuestion();
					}
				});
				pnlButtons.add(btnPrev);
			}
			{
				JButton btnCancel = new JButton("PRZERWIJ");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelFill();
					}
				});
				pnlButtons.add(btnCancel);
			}
		}
	}
	
	public PollFillDialog(String name, ClientRunner cl) {
		this();
		init(name);
		client = cl;

		cpcm = new ClientPanelContentManager();
		crm = new ClientResponseManager();

		crm.setPollName(name);

		cpcm.setPanel(pnlContent); // Dodaj panel
		cpcm.setRadioBtnGroup(btnGrp); // Dodaj ButtonGroup
		cpcm.setClientResponseManager(crm);
		cpcm.setClientRunner(cl);

		// Limit pytań
		questionLimit = p.getQuestions().size() - 1;
		loadQuestion(currentQuestionNumber);
		updateQuestionInfo();

		cpcm.setQuestion(currentQuestion);
		cpcm.transformAnswersToPanel();
	}
}