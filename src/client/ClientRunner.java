package client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;

import data.Poll;
import storage.StorageAdmin;

public class ClientRunner implements Runnable {
	Socket socket;
	BufferedReader reader = null;
	BufferedWriter writer = null;
	String ip;
	String serverIP;
	int port;
	boolean isRunning = true;
	
	ClientRunner(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}
	
	public synchronized void stop() {
		isRunning = false;
		try {
			if (socket != null) socket.close();
			reader.close();
			writer.close();
		} catch (Exception e) {
			socket = null;
			reader = null;
			writer = null;
		}
		serverIP = null;
	}
	void tryConnect() throws UnknownHostException, IOException {
		socket = new Socket(ip, port);
		writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		serverIP = socket.getRemoteSocketAddress().toString().substring(1);
	}
	
	synchronized String getServerIP() {
		return serverIP;
	}

	@Override
	public void run() {
		String line = null;
		while(isRunning) {
			try {
				line = reader.readLine();
			}
			catch(IOException e) {
				break;
			}
			if (line == null) {
				break;
			}
			processInput(line);
		}
	}
	
	synchronized void close() {
		try {
			socket.close();
		} catch (IOException e) {
			socket = null;
		}
	}
	void processInput(String line) {
		System.out.println("CLIENT PROCESS INPUT");
		Gson gson = new Gson();
		Poll p = gson.fromJson(line, Poll.class);
		StorageAdmin.savePollClient(p);
		System.out.println("Odebrano ankiete: " + p.getTitle());
	}
	
	synchronized void send(String line) {
		try {
			writer.write(line);
			writer.newLine();
			writer.flush();
			System.out.println("Wysłano odpowiedz: " + line);
		} catch (IOException e) {}
	}
}