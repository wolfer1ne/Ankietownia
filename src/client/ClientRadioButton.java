package client;

import javax.swing.JRadioButton;

/* Rozszerzona klasa RadioButton
 * Zawiera ID odpowiadającami pytania
 */
@SuppressWarnings("serial")
public class ClientRadioButton extends JRadioButton {
	Integer ID;
	
	public ClientRadioButton() {
		super();
		this.ID = -1;
	}

	public ClientRadioButton(String name, int iD) {
		super(name);
		this.ID = iD;
	}
	
	public Integer getRadioButtonID() {
		return ID;
	}
}