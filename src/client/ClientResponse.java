package client;

import java.util.ArrayList;
import java.util.List;

import data.Question;
import data.Question.QuestionType;

/*
 * Klasa zawierająca pojedyczny wpis z odpowiedziami
 */
public class ClientResponse {
	private int questionID; // ID pytania
	private QuestionType questionType; // Typ pytania
	private List <Integer> selectedAnswers; // Lista z ID odpowiedzi
	
	public List<Integer> getSelectedAnswers() {
		return selectedAnswers;
	}

	public ClientResponse() {
		questionID = -1;
		questionType = QuestionType.UNKNOWN;
		selectedAnswers = new ArrayList <Integer>();
	}
	
	public ClientResponse(Question q) {
		this();
		questionID = q.getID();
		questionType = q.getType();
	}

	public int getQuestionID() {
		return questionID;
	}

	public void setQuestionID(int questionID) {
		this.questionID = questionID;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}
	
	void resetSelectedAnswers() {
		selectedAnswers.clear();
	}

	// Zachowaj pojedyczna odpowiedz
	public void storeSingleEntry(int selectedButton) {
		resetSelectedAnswers();
		selectedAnswers.add(new Integer(selectedButton));
	}

	// Dodaj wielokrotna odpowiedz
	public void storeMultiEntry(int selectedCheckBox) {
		selectedAnswers.add(new Integer(selectedCheckBox));
	}
	
	public List<Integer> getMultiEntry() {
		if (selectedAnswers.isEmpty()) return null;
		return getSelectedAnswers();
	}

	public int getSingleEntry() {
		try {
			return selectedAnswers.get(0);
		} catch (Exception e) {
			return -1;
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[ClientResponse: qID(" + questionID
		+ ":" + Question.typeToString(questionType) + ")={");
		
		if (questionType == QuestionType.SINGLE) {
			sb.append(getSingleEntry() + "}");
		} else if (questionType == QuestionType.MULTIPLE) {
			if (getMultiEntry() != null) {
				for (Integer x : getMultiEntry()) {
					sb.append(x + " ");
				}
				sb.append("}");
			} else {
				sb.append("}");
			}
		}
		
		sb.append("]" + System.lineSeparator());
		return sb.toString();
	}
}