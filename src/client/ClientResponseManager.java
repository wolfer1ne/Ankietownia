package client;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/*
 * Klasa zarządzająca wpisami odpowiedzi
 */
public class ClientResponseManager {
	private transient String pollName;
	public String getPollName() {
		return pollName;
	}

	public void setPollName(String pollName) {
		this.pollName = pollName;
	}

	private transient String hostname;
	public String getHostname() {
		return hostname;
	}

	private transient String username;
	public String getUsername() {
		return username;
	}
	
	private String responseName = null;
	public void setResponseName() {
		responseName = "ANSWERS_" +
				getPollName() +
				"_" +
				getHostname() +
				"_" + getUsername() +
				ClientResponseManager.now() +
				".json";
	}
	
	public String getResponseName() {
		return responseName;
	}

	// Lista z wpisami
	private List <ClientResponse> clientResponses;
	
	public List<ClientResponse> getClientResponses() {
		return clientResponses;
	}

	// Format daty
	public static final transient String DATE_FMT= "yyyy_MM_dd_HH_mm_ss";
	
	public ClientResponseManager() {
		super();
		clientResponses = new ArrayList<ClientResponse>();
		hostname = getComputerName();
		username = System.getProperty("user.name");
	}

	// http://stackoverflow.com/a/33112997
	private String getComputerName()
	{
	    Map<String, String> env = System.getenv();
	    if (env.containsKey("COMPUTERNAME"))
	        return env.get("COMPUTERNAME");
	    else if (env.containsKey("HOSTNAME"))
	        return env.get("HOSTNAME");
	    else
	        return "Unknown Computer";
	}
	
	// http://stackoverflow.com/a/2942870
	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FMT);
		return sdf.format(cal.getTime());
	}

	// Zwróć wpis danej odpowiedzi
	public ClientResponse getResponseEntry(int questionID) {
		for (ClientResponse x : clientResponses) {
			if (x.getQuestionID() == questionID) return x;
		}
		return null;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[ClientResponseManager]" + System.lineSeparator());
		sb.append("\t" + hostname + System.lineSeparator());
		sb.append("\t" + username + System.lineSeparator());
		sb.append("\t" + now() + System.lineSeparator());
		
		for (ClientResponse cr : clientResponses) {
			sb.append("\t" + cr.toString());
		}
		
		sb.append(System.lineSeparator());
		return sb.toString();
	}
}