package storage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import client.ClientResponseManager;

public class StorageClient {
	// Zapisuje plik z odpowiedziami (Klient)
	public static boolean saveResponse(ClientResponseManager crm) {
		String directory = StorageDirectory.getClientAnswersDir() + File.separator;
		
		String outputName =
				"ANSWERS_" +
				crm.getPollName() +
				"_" +
				crm.getHostname() +
				"_" + crm.getUsername() +
				ClientResponseManager.now() +
				".json";
		
		try (Writer writer = new FileWriter(directory + outputName)) {
		    Gson gson = new GsonBuilder().create();
		    gson.toJson(crm, writer);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}