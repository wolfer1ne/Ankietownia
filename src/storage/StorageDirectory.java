package storage;

import java.io.File;

public class StorageDirectory {
	public static final String ServerPollDir = "ANKIETY";	
	public static final String ClientPollDir = "dane";
	public static final String ClientAnswersDir = ClientPollDir + File.separator + "odpowiedzi";
	public static final String ServerAnswersDir = ServerPollDir + File.separator + "odpowiedzi";
	
	StorageDirectory() {
		StorageDirectory.getServerStorageDirectory();	
	}
	
	public static final String getServerStorageDirectory() {
		return getDir(ServerPollDir);
	}
	
	public static final String getClientStorageDirectory() {
		return getDir(ClientPollDir);
	}
	
	public static final String getClientAnswersDir() {
		return getDir(ClientAnswersDir);
	}
	
	public static final String getServerAnswersDir() {
		return getDir(ServerAnswersDir);
	}
	
	private static final String getDir(String name) {
		File f = new File(name);
		if (!f.isDirectory()) {
		   f.mkdir();
		}
		
		return f.getAbsolutePath();
	}
}