package storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import client.ClientResponseManager;
import data.Poll;

public class StorageAdmin {
	// Zwraca liste nazw ankiet w puli serwera
	public static List<String> getServerPollsNames() {
		return getPollsNames(StorageDirectory.getServerStorageDirectory());
	}
	
	// Zwraca liste nazw ankiet w puli klienta
	public static List<String> getClientPollsNames() {
		return getPollsNames(StorageDirectory.getClientStorageDirectory());
	}
	
	// Zwraca liste nazw ankiet
	// Przyjmuje sciezke do folderu z ankietami
	private static List<String> getPollsNames(String name) {
		List<String> polls = new ArrayList<>();
		
		File dir = new File(name);
		File[] filesList = dir.listFiles();
		for (File file : filesList) {
		    if (file.isFile()) {
		        polls.add(file.getName().
		        		substring(0, file.getName().lastIndexOf('.'))
);
		    }
		}
		
		return polls;
	}

	public static void savePoll(Poll p) {
		savePollToJSON(p);
	}

	// Usuwa ankiete w puli serwera
	public static boolean removeServerPoll(String p) {
		return removePoll(p, StorageDirectory.getServerStorageDirectory());
	}
	
	// Usuwa ankiete w puli klienta
	public static boolean removeClientPoll(String p) {
		return removePoll(p, StorageDirectory.getClientStorageDirectory());
	}

	// Usuwa ankiete w puli
	// Przyjmuje nazwe i sciezke do folderu z ankietami
	private static boolean removePoll(String name, String dir) {
		String input = dir + File.separator + name + ".json";
		return (new File(input).delete());
	}

	static void sendPoll() {}
	
	public static Poll getServerPoll(String name) {
		return loadPollFromJSON(name, StorageDirectory.getServerStorageDirectory());
	}
	
	public static Poll getClientPoll(String name) {
		return loadPollFromJSON(name, StorageDirectory.getClientStorageDirectory());
	}

	static Poll loadPollFromJSON(String name, String dir) {
		String input = dir + File.separator	+ name + ".json";
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(input));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		Poll p = gson.fromJson(br, Poll.class);
		return p;
	}

	static boolean savePollToJSON(Poll p) {
		String output = StorageDirectory.getServerStorageDirectory()
				+ File.separator
				+ p.getTitle() + ".json";
		
		try (Writer writer = new FileWriter(output)) {
		    Gson gson = new GsonBuilder().create();
		    gson.toJson(p, writer);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean savePollClient(Poll p) {
		String dir = StorageDirectory.getClientStorageDirectory()
				+ File.separator + p.getTitle() + ".json";
		
		try (Writer writer = new FileWriter(dir)) {
		    Gson gson = new GsonBuilder().create();
		    gson.toJson(p, writer);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean serverSaveResponse(ClientResponseManager crm) {
		String dir = StorageDirectory.getServerAnswersDir()
				+ File.separator + crm.getResponseName();
		
		try (Writer writer = new FileWriter(dir)) {
		    Gson gson = new GsonBuilder().create();
		    gson.toJson(crm, writer);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}